# Experimental PKGBUILDs

This folder contains experimental PKGBUILDs to install software to Arch Linux ARM that's not already in the AUR.

Please use with care and report or fix PKGBUILD related issues. Thank you!

If it works for you and you have checked that the PKGBUILD follows the guidelines, feel free to submit my PKGBUILDs upstream!
